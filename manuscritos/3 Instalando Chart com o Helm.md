# Instalando chart com o Helm

Para procurar chart, utilize o repositório padrão <https://artifacthub.io/>

Por exemplo vamos instalar o ingress controller o do nginx procurando no link.

![nginxfind](../pics/chartnginx.png)

Sempre observe se o repositório é o oficial, a quantidade de estrelas, se é um repo que sofre atualiação, etc. 

Entrando nesse chart podemos ver dois tipos de instalação <https://artifacthub.io/packages/helm/nginx/nginx-ingress>

Podemos observar duas coisas. Podemos clonar o projeto que gerou o chart, para que possamos baseado neste chart gerar um subchart (um chart dependente de outro) e também podemos somente seguir o fluxo e adicionar o repositório. Vamos somente fazer o báisco e adicionar o repositório com os comandos que ele mesmo mostrou..

````bash
# nginx-stable é o nome que vai vai dar ao repo e poderia ser qualquer coisa
helm repo add nginx-stable https://helm.nginx.com/stable
# É necessário atualizar para que esse repo seja usado pelo helm.
helm repo update
````

Antes de instalar, vamos ver o que temos nesse repo que não somente é um chart, mas alguns. Aqui já começamos a entender o repo para o chart.

````bash
❯ helm search repo nginx     
NAME                                            CHART VERSION   APP VERSION     DESCRIPTION                                      
nginx-stable/nginx-appprotect-dos-arbitrator    0.1.0           1.1.0           NGINX App Protect Dos arbitrator                 
nginx-stable/nginx-devportal                    1.3.1           1.3.1           A Helm chart for Kubernetes
# Olha ele aqui                      
nginx-stable/nginx-ingress                      0.15.2          2.4.2           NGINX Ingress Controller                         
nginx-stable/nginx-service-mesh                 0.6.0           1.6.0           NGINX Service Mesh                               
nginx-stable/nms                                1.0.0           1.0.0           A chart for installing the NGINX Management Suite
nginx-stable/nms-acm                            1.3.1           1.3.1           A Helm chart for Kubernetes                      
nginx-stable/nms-hybrid                         2.6.0           2.6.0           A Helm chart for Kubernetes  
````

Se olharmos a documentação do chart veremos que a instalação referencia esse remsmo chart. O nome do repo é nginx-stable e o chart é o nginx-ingress.

Se vc executar a instalação com o método abaixo, vc estará executando a instalação padrão com os Values.yaml no default.

````bash
helm install my-release nginx-stable/nginx-ingress
````

Para você analisar o Values.yaml na própria página do projeto temos o Default Values para ser analisado bem como outros detalhes.

![details](../pics/chartnginxdetails.png)
![detailsvalues](../pics/chartnginxdetailsvalues.png)

>Em alguns projetos esta e outros não está habilitados, mas com o comando do helm show já podemos ver esses valores default.

````bash
helm show values nginx-stable/nginx-ingress > ./resources/Values.yaml
````

De uma olhada nesse arquivo e veja o que vc poderia mudar, caso quisesse.

Para instalar passando um arquivo de values podemos utilizar o -f.

````bash
helm install nginx-ingress nginx-stable/nginx-ingress -f ./resources/Values.yaml  
#NAME: nginx-ingress
#LAST DEPLOYED: Mon Dec 26 01:20:59 2022
#NAMESPACE: default
#STATUS: deployed
#REVISION: 1
#TEST SUITE: None
#NOTES:
#The NGINX Ingress Controller has been installed.
````

Também poderíamos mudar somente um parametro no arquivo de value default sem passar de fato o arquivo todo, com o set para setar somente algum parâmetro.

Por exemplo vamos alterar de deployment para daemonset.
Podemos ver isso na configuração

````yaml
  ## The kind of the Ingress Controller installation - deployment or daemonset.
  kind: deployment
````

Já que não estamos instalando, podemos fazer somente um upgrade. Observe que a revisão vai subir, pois já é a segunda vez que estamos aplicando. Ele faz isso para poder fazer o rollback caso necessário.

````bash
helm upgrade nginx-ingress --set kind=daemonset nginx-stable/nginx-ingress 
#Release "nginx-ingress" has been upgraded. Happy Helming!
#NAME: nginx-ingress
#LAST DEPLOYED: Mon Dec 26 01:25:48 2022
#NAMESPACE: default
#STATUS: deployed
#REVISION: 2
#TEST SUITE: None
#NOTES:
#The NGINX Ingress Controller has been installed.
````

Delete é mole também

````bash
helm uninstall #nginx-ingress                                      
#release "nginx-ingress" uninstalled
````

Se observar ele criou tudo anteriormente em um namespace que estava no momento no caso default. Agora vamos aplicar novamente passando o parametro para criar um namepace basedo no nome que demos para a instalação do helm que foi nginx-ingress

````bash
# --create-namespace irá criar caso não exista o --namespace passado
helm install nginx-ingress nginx-stable/nginx-ingress --values ./resources/Values.yaml --create-namespace --namespace nginx-ingress
#NAME: nginx-ingress
#LAST DEPLOYED: Mon Dec 26 01:54:55 2022
#NAMESPACE: nginx-ingress
#STATUS: deployed
#REVISION: 1
#TEST SUITE: None
#NOTES:
#The NGINX Ingress Controller has been installed.
````

Eu particulamente gosto de ter o arquivo Values.yaml definido para lembrar de fato o que foi aplicado e futuramente usar gitops para essa controle.

Para verificar o que temos criado com o helm.

````bash
helm list
NAME            NAMESPACE       REVISION        UPDATED                                 STATUS          CHART                   APP VERSION
nginx-ingress   nginx-ingress   1               2022-12-26 02:06:02.518055414 -0300 -03 deployed        nginx-ingress-0.15.2    2.4.2  
````
