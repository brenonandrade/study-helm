# Conceitos Básicos

## Publicação no kubernetes

Algumas etapas são necessárias entender sobre o kubernetes para depois aprender o Helm.

O que é necessário para publicar um app no kubernetes?

- Imagem de container
      Um container precisa de alguma imagem para executar, logo podemos referenciar uma já existente ou alguma que criamos anteriormente
- Manifestos yaml
  - É necessário para criar os objetos que irão trabalhar com o app no kubernetes. deployment, replicaset, services, ingress, service account, etc
- kubectl create
  - para aplicar os manifestos yaml acima citados.

Vamos imaginar um sistema com vários microserviços: api, banco de dados no kubernetes, algum serviço pubsub, etc

![eshop](../pics/exampleeshop.png)

Podemos observar que existe uma complexidade grande para implantar esses microserviços pois cada um terá seu próprio conjunto de manifestos que irão gerar objetos com diferentes configurações.

Para piorar, caso queira implantar em outros cluster kubernetes será necessário alterar todo o set de configuração, e fazer isso com os manifestos fica um processo muito trabalhoso e inviável, pois são muitos objetos.

>Ai vem a pergunta, como simplificar isso? Helm que é um gerenciador de pacotes para kubernetes.

Com o Helm, você consegue instalar aplicações complexas no cluster kubernetes de uma forma muito mais simples, baseado em repositório.

Para simplificar, poderíamos comparar ao apt(debian) yum (red hat). Geralmente os comandos são os mesmo desses gerenciadores

````bash
# para add um repo
helm repo add
# procurar um repo
helm search repo
# instalar um chart
helm install
# fazer o update do chart
helm update
# caso queira voltar uma aplicacao
helm rollback
# criar um chart
helm create
# verificar se esta instalado
helm status
````

Assim como qualquer gerenciado de pacote tem os repositórios para encontrar os pacotes, no helm também é necessários adicionar os repositórios para encontrar os charts.

## Repositório

É onde guarda os seus charts

## Charts

O `chart` é o projeto que você gostaria de fazer a entrega. É a estrutura de entrega da sua aplicação, `um template`. Um repo pode ter vários charts. Vamos imaginar um chart que vai aplicar sem alta disponibilidade e outro que vai aplicar com alta disponibilidade um entrega. São projetos diferentes.

<img src="../pics/demochart.png" width="300">

Existem dois arquivos super importantes:

- Chart.yaml
  - Contém a especificação do chart, os metadados
- Values.yaml
  - Contém os valores padrões para fazer a entrega, que podem ser sobreescritos caso queira mudar algum elemento de configuração.
- templates que irão receber estes valores e irá montar os yaml para aplicar no cluster kubernetes
- arquivos .tpl que são auxiliares aos templates
- NOTES.txe
  - Exibe os dados depois que a instalação foi feita, ou possui algum helper depois que a instalação foi feita.

Depois de deployments.yaml por exemplo, ao invés de vc ter referencia a um valor estático, este será referenciado a um valor do Values.yaml o que permitirá uma possível mudança caso a pessoa queira.

## Release

A release é uma versão do chart

